<?php


namespace LaraStudy\Garbage;

trait HelperTrait
{
    public function getName()
    {
        return $this->name;
    }

    public function getAge()
    {
        return $this->age;
    }
}