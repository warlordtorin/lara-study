<?php


namespace LaraStudy\Garbage\Cache3;


use LaraStudy\Garbage\Cache3\Exceptions\CacheDriverConfigException;
use LaraStudy\Garbage\Cache3\Exceptions\CacheDriverException;

class CacheDriver
{

    protected static array $drivers = [];

    /**
     * @throws CacheDriverException
     * @throws CacheDriverConfigException|Exceptions\CacheDriverConfigException
     */
    public static function factory(string $type): CacheDriverInterface
    {
        self::load();

        switch ($type) {
            case "memory":
                return new MemoryCacheDriver(self::$drivers['drivers'][$type]);
            case "file":
                return new FileCacheDriver(self::$drivers['drivers'][$type]);
            case "database":
                return new DbCacheDriver(self::$drivers['drivers'][$type]);
        }

        throw new CacheDriverException("Cannot get driver: $type");
    }

    protected static function load()
    {
        self::$drivers = include './config.php';
    }
}