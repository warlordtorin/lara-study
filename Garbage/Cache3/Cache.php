<?php


namespace LaraStudy\Garbage\Cache3;


class Cache
{
    protected CacheDriverInterface $driver;

    protected function __construct(CacheDriverInterface $driver)
    {
        $this->driver = $driver;
    }

    /**
     * @param string $type
     * @return Cache
     * @throws CacheDriverException
     * @throws CacheDriverConfigException
     */
    public static function storage(string $type): Cache
    {
        return new static(CacheDriver::factory($type));
    }

    /**
     * @param string $key
     * @return mixed
     */
    public function get(string $key): mixed
    {
        return $this->driver->get($key);
    }

    /**
     * @param string $key
     * @param mixed $value
     */
    public function set(string $key, mixed $value): void
    {
        $this->driver->set($key, $value);
    }
}