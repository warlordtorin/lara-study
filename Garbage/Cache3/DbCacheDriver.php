<?php


namespace LaraStudy\Garbage\Cache3;


use LaraStudy\Garbage\Cache3\Exceptions\CacheDriverConfigException;

class DbCacheDriver implements CacheDriverInterface
{
    /**
     * @var array
     */
    protected array $config = [];

    /**
     * @throws CacheDriverConfigException
     */
    public function __construct(array $config)
    {
        $this->loadConfig($config);
    }

    /**
     * @param string $key
     * @param mixed $value
     */
    public function set(string $key, mixed $value): void
    {
        // TODO: Implement set() method.
    }

    /**
     * @param string $key
     * @return mixed
     */
    public function get(string $key): mixed
    {
        // TODO: Implement get() method.
    }

    /**
     * @throws CacheDriverConfigException
     */
    public function loadConfig(array $config)
    {
        try {
            $this->config['database'] = $config['database'];
            $this->config['host'] = $config['host'];
            $this->config['port'] = $config['port'];
            $this->config['user'] = $config['user'];
            $this->config['password'] = $config['password'];

        } catch (\Exception $exception) {
            throw new CacheDriverConfigException($exception->getMessage());
        }
    }
}