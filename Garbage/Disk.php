<?php


namespace LaraStudy\Garbage;


class Disk implements Figure
{
    private $radius;

    public function __construct($radius)
    {
        $this->radius = $radius;
    }

    public function getPerimeter()
    {
        return 2 * 3.14 * $this->radius;
    }

    public function getSquare()
    {
        return 3.14 * $this->radius ^ 2;
    }


}