<?php


namespace LaraStudy\Garbage\Cache1;


use LaraStudy\Garbage\Cache1\Exceptions\CacheDriverException;

class Cache
{
    protected CacheDriverInterface $driver;

    protected function __construct(CacheDriverInterface $driver)
    {
        $this->driver = $driver;
    }

    /**
     * @param string $type
     * @return Cache
     * @throws CacheDriverException|Exceptions\CacheDriverConfigException
     */
    public static function storage(string $type, array $config = []): Cache
    {
        switch ($type) {
            case "memory":
                return new static((new MemoryCacheDriver($config)));
            case "file":
                return new static((new FileCacheDriver($config)));
            case "database":
                return new static((new DbCacheDriver($config)));
        }

        throw new CacheDriverException("Cannot get driver: $type");
    }

    /**
     * @param string $key
     * @return mixed
     */
    public function get(string $key): mixed
    {
        return $this->driver->get($key);
    }

    /**
     * @param string $key
     * @param mixed $value
     */
    public function set(string $key, mixed $value): void
    {
        $this->driver->set($key, $value);
    }
}