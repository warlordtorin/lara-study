<?php


namespace LaraStudy\Garbage;


interface Figure
{
    public function getSquare();

    public function getPerimeter();
}