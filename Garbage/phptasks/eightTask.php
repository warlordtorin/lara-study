<?php

/**
 * @param $string
 * @return string
 */
function stingRegistryChanger($string)
{
    $splited = str_split($string);
    $first_char = false;
    $i = 0;
    foreach ($splited as $char) {
        if (preg_match('/\s+/', $char)) {
            if (!$first_char) {
                $splited[$i] = strtoupper($char);
                $first_char = true;
            } else {
                $splited[$i] = strtolower($char);
            }
        }
        $i++;
    }
    return implode($splited);
}