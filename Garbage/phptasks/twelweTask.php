<?php

function parseXml(string $xmlString, array $tags = [], $level = 0)
{
    $result_arr = [];
    $arrayed_xml = str_split($xmlString);
    $openedTagPosition = 0;
    $closedTagPosition = 0;

    foreach ($arrayed_xml as $key => $item) {
        if ($item === '<' && $arrayed_xml[$key + 1] !== '/') {
            $openedTagPosition = $key;
        }
        if ($item === '>') {
            $closedTagPosition = $key;
        }
        if ($openedTagPosition !== 0 && $closedTagPosition !== 0) {
            $concatedTag = [];
            for ($i = $openedTagPosition + 1; $i < $closedTagPosition; $i++) {
                $concatedTag[] = $arrayed_xml[$i];
            }
            $implodedTag = implode($concatedTag);
            if (in_array($implodedTag, $tags)) {
                $next_level = implode(array_slice($arrayed_xml, $closedTagPosition + 1, -($closedTagPosition)));
                $result_arr +=
                    [
                        'tag' => $implodedTag,
                        'text' => '',
                        'children' => parseXml($next_level, $tags, $level++)
                    ];
            } else {
                $openedTagPosition = 0;
                $closedTagPosition = 0;
            }
        }
    }
    return $result_arr;
}

function checkIfNeededTag(string $tagToValidate, array $validTags)
{

}