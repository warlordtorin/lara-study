<?php

/**
 * @param array $searchableArray
 * @param $needle
 * @param $currentKey
 * @param $strict
 * @return false|string
 */
function arraySearch(array $searchableArray, $needle, $currentKey, $strict)
{
    foreach ($searchableArray as $key => $value) {
        if (is_array($value)) {
            $nextKey = $this->arraySearch($value, $needle, $currentKey . '[' . $key . ']', $strict);
            if ($nextKey) {
                return $nextKey;
            }
        } elseif (!$strict && $value == $needle) {
            return is_numeric($key) ? $currentKey . '[' . $key . ']' : $currentKey . '["' . $key . '"]';
        } elseif ($strict && $value === $needle) {
            return is_numeric($key) ? $currentKey . '[' . $key . ']' : $currentKey . '["' . $key . '"]';
        }
    }
    return false;
}