<?php


namespace LaraStudy\Garbage;


class CollectionEleventhTask
{
    /**
     * @var array
     */
    protected $items;

    protected function __construct($items)
    {
        $this->items = $items;
    }

    /**
     * @param array $array
     * @return static
     */
    public static function make(array $array = [])
    {
        return (new static($array));
    }

    /**
     * @param callable $callback
     * @return static
     */
    public function map(callable $callback)
    {
        $items = array_map($callback, $this->items);

        return new static($items);
    }

    /**
     * @param callable|null $callback
     * @return static
     */
    public function filter(callable $callback = null)
    {
        $items = array_filter($this->items, $callback);

        return new static($items);
    }

    /**
     * @return int
     */
    public function count()
    {
        return count($this->items);
    }

    /**
     * @return array
     */
    public function values()
    {
        return array_values($this->items);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return $this->values();
    }
}
