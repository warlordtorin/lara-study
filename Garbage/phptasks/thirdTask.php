<?php

/**
 * @param array $firstArray
 * @param array $secondArray
 * @param $reverseFlag
 * @return array
 */
function concatenateArrays(array $firstArray, array $secondArray, $reverseFlag)
{
    $keyArr = $firstArray;
    $valArr = $secondArray;
    $resultArray = [];
    if ($reverseFlag) {
        $keyArr = $secondArray;
        $valArr = $firstArray;
    }
    $i = 1;
    foreach ($keyArr as $value) {
        $keyArrCount = count($keyArr);
        $valArrCount = count($valArr);
        $keyValDiff = $keyArrCount - $valArrCount;
        switch ($keyValDiff) {
            case $keyValDiff > 0:
                if ($keyArrCount !== $i) {
                    $resultArray[$value] = $valArr[$i];
                } else {
                    $resultArray[$value] = '';
                }
                break;
            case $keyValDiff < 0:
                if ($valArrCount !== $i) {
                    $resultArray[$value] = $valArr[$i];
                } else {
                    for ($j = 0; $j <= $keyValDiff; $j--) {
                        $resultArray[$i] = $valArr[$i];
                    }
                }
                break;
            default:
                $resultArray[$value] = $valArr[$i];
                break;
        }
        $i++;
    }
    return $resultArray;
}