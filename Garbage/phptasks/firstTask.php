<?php

/**
 * @param array $arrayToFilter
 * @param $deletingValue
 */
function deleteValueUsingArrayFilter(array $arrayToFilter, $deletingValue)
{
    array_filter($arrayToFilter, function ($value) use ($deletingValue) {
        if ($value === $deletingValue) {
            return false;
        }
        return true;
    });
}