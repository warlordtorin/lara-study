<?php

/**
 * @param $needle
 * @param $haystack
 * @param $strict
 * @param int $inArrayCount
 * @return int
 */
function checkPhraseCount($needle, $haystack, $strict, $inArrayCount = 0)
{
    foreach ($haystack as $key => $value) {
        if (is_array($value)) {
            $inArrayCount += checkPhraseCount($needle, $value, $strict, $inArrayCount);
        }
        if ($strict ? $value === $needle : $value == $needle) {
            $inArrayCount++;
        }
    }
    return $inArrayCount;
}