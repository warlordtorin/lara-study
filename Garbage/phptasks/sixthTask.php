<?php

/**
 * @param $html
 * @param array $context
 * @return mixed|string|string[]|null
 */
function htmlParser($html, array $context)
{
    foreach ($context as $key => $item) {
        $html = preg_replace('/{{\$' . $key . '}}/', $item, $context);
    }
    return $html;
}