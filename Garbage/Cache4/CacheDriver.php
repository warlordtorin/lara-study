<?php


namespace LaraStudy\Garbage\Cache4;


use LaraStudy\Garbage\Cache4\Exceptions\CacheDriverException;


class CacheDriver
{

    protected static array $drivers = [];

    /**
     * @throws CacheDriverException
     * @throws Exceptions\CacheDriverConfigException
     */
    public static function factory(string $type): CacheDriverInterface
    {
        switch ($type) {
            case "memory":
                return new MemoryCacheDriver(self::$drivers['drivers'][$type]);
            case "file":
                return new FileCacheDriver(self::$drivers['drivers'][$type]);
            case "database":
                return new DbCacheDriver(self::$drivers['drivers'][$type]);
        }

        throw new CacheDriverException("Cannot get driver: $type");
    }

    public static function load($configPath)
    {
        self::$drivers = include $configPath;
    }
}