<?php


namespace LaraStudy\Garbage\CacheNineTask;


interface CacheDriverInterface
{
    /**
     * @param $key string
     * @return mixed
     */
    public function get(string $key);

    /**
     * @param $key string
     * @param $value mixed
     * @return void
     */
    public function set(string $key, $value);
}