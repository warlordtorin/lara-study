<?php


namespace LaraStudy\Garbage\CacheNineTask;


class DbService
{
    /**
     * @var string
     */
    private $db_host;

    /**
     * @var string
     */
    private $db_username;

    /**
     * @var string
     */
    private $db_password;

    /**
     * DbService constructor.
     * @param string $db_host
     * @param string $db_username
     * @param string $db_password
     */
    public function __construct(string $db_host, string $db_username, string $db_password)
    {
        $this->db_host = $db_host;
        $this->db_username = $db_username;
        $this->db_password = $db_password;
    }

    /**
     * @return array
     */
    public function getСonfig()
    {
        return
            [
                'db_host' => $this->db_host,
                'db_username' => $this->db_username,
                'db_password' => $this->db_password
            ];
    }
}