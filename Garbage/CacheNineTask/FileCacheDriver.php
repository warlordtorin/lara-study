<?php


namespace LaraStudy\Garbage\CacheNineTask;


class FileCacheDriver implements CacheDriverInterface
{
    protected string $storage = 'storage';

    public function __construct()
    {
        $this->storage = realpath(
            __DIR__ .
            DIRECTORY_SEPARATOR .
            $this->storage
        );
    }

    public function get(string $key)
    {

        return \json_decode(
            file_get_contents($this->makePath($key))
        );
    }

    public function set(string $key, $value)
    {
        file_put_contents(
            $this->makePath($key),
            \json_encode($value)
        );
    }

    protected function makePath(string $key): string
    {
        $file = md5($key);
        return $this->storage . DIRECTORY_SEPARATOR . $file;
    }
}