<?php

namespace LaraStudy\Garbage\CacheNineTask;

use LaraStudy\Garbage\CacheNineTask\CacheDriverInterface;


class Cache
{
    protected CacheDriverInterface $driver;

    protected $dbConfig;

    protected function __construct($driver)
    {
        $this->driver = $driver;
        $config = new DbService('', '', '');
        $this->dbConfig = $config->getСonfig();
    }

    /**
     * @param $type
     * @return Cache
     * @throws \Exception
     */
    public static function storage($type): Cache
    {
        switch ($type) {
            case "memory":
                return new self((new MemoryCacheDriver()));
            case "file":
                return new self((new FileCacheDriver()));
            case "database":
                return new self((new DbCacheDriver()));
        }

        throw new \Exception('Unknown driver: ' . $type);
    }

    /**
     * @param $key string
     * @return mixed
     */
    public function get(string $key)
    {
        return $this->driver->get($key);
    }

    /**
     * @param $key string
     * @param $value mixed
     * @return void
     */
    public function set(string $key, $value)
    {
        $this->driver->set($key, $value);
    }
}