<?php


namespace LaraStudy\Garbage\CacheNineTask;


class MemoryCacheDriver implements CacheDriverInterface
{

    protected static $db = [];

    public function get(string $key)
    {
        return self::$db[$key] ?? null;
    }

    public function set(string $key, $value)
    {
        self::$db[$key] = $value;
    }
}