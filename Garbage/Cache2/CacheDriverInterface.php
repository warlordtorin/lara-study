<?php


namespace LaraStudy\Garbage\Cache2;


interface CacheDriverInterface
{
    /**
     * @param string $key
     * @param mixed $value
     */
    public function set(string $key, mixed $value): void;

    /**
     * @param string $key
     * @return mixed
     */
    public function get(string $key): mixed;
}