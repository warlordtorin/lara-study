<?php


namespace LaraStudy\Garbage\Cache2;


class CacheDriver
{
    /**
     * @throws CacheDriverException
     * @throws CacheDriverConfigException
     */
    public static function factory(string $type): CacheDriverInterface
    {
        switch ($type) {
            case "memory":
                return new MemoryCacheDriver($config);
            case "file":
                return new FileCacheDriver($config);
            case "database":
                return new DbCacheDriver($config);
        }

        throw new CacheDriverException("Cannot get driver: $type");
    }
}