<?php


namespace LaraStudy\Garbage\Cache2;


class MemoryCacheDriver implements CacheDriverInterface
{
    static array $storage = [];

    /**
     * @var array
     */
    protected array $config = [];

    public function __construct(array $config)
    {
        $this->config = $config;
    }

    /**
     * @param string $key
     * @param mixed $value
     */
    public function set(string $key, mixed $value): void
    {
        self::$storage[$key] = $value;
    }

    /**
     * @param string $key
     * @return mixed
     */
    public function get(string $key): mixed
    {
        return self::$storage[$key] ?? null;
    }

    public function loadConfig(array $config)
    {
        // INFO: Not need
    }
}