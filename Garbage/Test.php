<?php


namespace LaraStudy\Garbage;


class Test
{
    use Trait1, Trait2, Trait3{
        Trait1::method insteadof Trait2;

        Trait1::method as public;
        Trait1::method as aliasMethod1;
        Trait2::method as aliasMethod2;
        Trait3::method as aliasMethod3;
    }

    public function getSum()
    {
        return $this->method1() + $this->method2() + $this->method3();
    }

    public function getSumAliased(){
        return $this->aliasMethod1() + $this->aliasMethod2() + $this->aliasMethod3();
    }

}

$test = new Test();
var_dump($test->getSum());
var_dump($test->getSumAliased());